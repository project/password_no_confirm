This module removes the password confirmation field on the new user registration
form to expedite registration.

Just enable this module for the change to take effect. No configuration is required.
